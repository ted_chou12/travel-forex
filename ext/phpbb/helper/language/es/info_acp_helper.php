<?php
/**
*
* @package phpbb Helper
* @copyright (c) 2015 phpbb.com
* @license Proprietary
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'FH_DISABLE_ALL'		=> 'Deshabilitar todas las extensiones', 

	'LOG_CORE_INSTALLED'	=> '<strong>phpbb Helper</strong><br />» Archivos cambiados correctamente',
	'LOG_CORE_DEINSTALLED'	=> '<strong>phpbb Helper</strong><br />» Archivos restaurados correctamente',
	'LOG_CORE_NOT_REPLACED'	=> '<strong>phpbb Helper</strong><br />» No se pudo reemplazar el/los archivo(s)<br />» %s',
	'LOG_CORE_NOT_UPDATED'	=> '<strong>phpbb Helper</strong><br />» No se pudo reemplazar el/los archivo(s)<br />» %s',

	'ERROR_DISABLE'			=> 'No se puede deshabilitar phpbb Helper debido a que hay extensiones de phpbb habilitadas<br /><div style="margin: 0px auto; width: 50%%; text-align: left;">» %s</div>'
));
