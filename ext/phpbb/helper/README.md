phpbb helper
===========
phpbb helper contains helpermodules for your phpbb extensions. You aren't able to enable phpbb extensions without this module. It will enable itself once installed.

## Requirements
* phpBB 3.1.0 or higher
* PHP 5.3.3 or higher

## Quick Installation
You can quickly install this extension on the latest version of [phpBB 3.1](https://www.phpbb.com/downloads/) or on the latest development version of [phpBB 3.2-dev](https://github.com/phpbb/phpbb3) by doing the following:

1. Upload the extension with "[Upload Extensions](https://github.com/phpbb/upload)".
2. Check that you have uploaded the correct files.
3. Click `Enable`.

## Standard Installation
You can install this extension on the latest version of [phpBB 3.1](https://www.phpbb.com/downloads/) or on the latest development version of [phpBB 3.1-dev](https://github.com/phpbb/phpbb3) by doing the following:

1. Download the extension. You can do it by downloading the [latest ZIP-archive of `master` branch of its GitHub repository](https://github.com/phpbb/helper/archive/master.zip).
2. Check out the existence of the folder `/ext/phpbb/helper/` in the root of your board folder. Create folders if necessary.
3. Copy the contents of the downloaded `helper-master` folder to `/ext/phpbb/helper/`.
4. Navigate in the ACP to `Customise -> Extension Management -> Manage extensions -> phpbb helper`.
5. Click `Enable`.

## Update
1. Download the updated extension. You can do it by downloading the [latest ZIP-archive of `master` branch of its GitHub repository](https://github.com/phpbb/helper/archive/master.zip).
2. Navigate in the ACP to `Customise -> Extension Management -> Manage extensions -> phpbb helper` and click `Disable`. Only possible when no phpbb extensions are active.
3. Copy the contents of the downloaded `helper-master` folder to `/ext/phpbb/helper/`.
4. Navigate in the ACP to `Customise -> Extension Management -> Manage extensions -> phpbb helper` and click `Enable`.
5. Click `Details` or `Re-Check all versions` link to follow updates.

## Uninstallation
Navigate in the ACP to `Customise -> Extension Management -> Manage extensions -> phpbb helper` and click `Disable`. You only can disable when no phpbb extensions are active anymore.

For permanent uninstallation click `Delete Data` and then you can safely delete the `/ext/phpbb/helper/` folder.

We feel sorry as our answers on phpbb sites are removed, so use github or our forum for answers.

## License
[GNU General Public License v2](http://opensource.org/licenses/GPL-2.0)

© 2014 - John Peskens (http://phpbb.com)