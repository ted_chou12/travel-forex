<?php

// bookmark . ted - create rate api

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'includes/functions_display.' . $phpEx);


// api constants
$app_id = '5b822fda89c04bc0ae437a75e3faa92f';
$url = 'https://openexchangerates.org/api/latest.json';
$data = array('app_id' => $app_id);
$params = array();
foreach ($data as $key => $val) {
  $params[] = $key.'='.$val;
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url.'?'.implode('&', $params));
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);
curl_close($ch);

//bookmark . ted - write file
// $file = fopen('assets/latest_rates.json', 'w');
// echo fwrite($file, $response);
// fclose($file);

$data = json_decode($response, true);
$latest_rate = round($data['rates']['TWD'] / $data['rates']['JPY'], 4);


$sql = 'INSERT INTO ' . RATE_TABLE . ' ' . $db->sql_build_array('INSERT', array(
  'rate_datetime'		=> date('Y-m-d H:i:s'),
  'rate_twd'		=> (float) round($data['rates']['TWD'], 4),
  'rate_jpy'		=> (float) round($data['rates']['JPY'], 4),
  'rate_price'		=> (float) $latest_rate)
);
$db->sql_query($sql);


 ?>
