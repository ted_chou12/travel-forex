<?php
namespace phpbb\cron\task\core;

/**
 * Update old hashes to the current default hashing algorithm
 *
 * It is intended to gradually update all "old" style hashes to the
 * current default hashing algorithm.
 */
class update_rate extends \phpbb\cron\task\base
{
	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/**
	 * Constructor.
	 *
	 * @param \phpbb\config\config $config
	 * @param \phpbb\db\driver\driver_interface $db
	 * @param \phpbb\lock\db $update_lock
	 * @param \phpbb\passwords\manager $passwords_manager
	 * @param array $hashing_algorithms Hashing driver
	 *			service collection
	 * @param array $defaults Default password types
	 */
	public function __construct(\phpbb\config\config $config, \phpbb\db\driver\driver_interface $db, \Symfony\Component\DependencyInjection\ContainerInterface $phpbb_container)
	{
		$this->config = $config;
		$this->db = $db;
		$this->phpbb_container = $phpbb_container;
	}

	/**
	* Runs this cron task.
	*
	* @return null
	*/
	public function run()
	{
		// api constants
		$app_id = '5b822fda89c04bc0ae437a75e3faa92f';
		$url = 'https://openexchangerates.org/api/latest.json';
		$data = array('app_id' => $app_id);
		$params = array();
		foreach ($data as $key => $val) {
		  $params[] = $key.'='.$val;
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url.'?'.implode('&', $params));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);

		$data = json_decode($response, true);
		$latest_rate = round($data['rates']['TWD'] / $data['rates']['JPY'], 4);

		$rate = $this->phpbb_container->get('rate');

		$rate->update_rate($data['rates']['TWD'], $data['rates']['JPY'], $latest_rate);

		$this->config->set('update_rate_last_gc', time());
		$this->config->set('update_rate_gc', 3600);
	}

	/**
	* Returns whether this cron task can run, given current board configuration.
	*
	* This cron task will only run when system cron is utilised.
	*
	* @return bool
	*/
	public function is_runnable()
	{
		return true;
	}

	/**
	* Returns whether this cron task should run now, because enough time
	* has passed since it was last run.
	*
	* The interval between database tidying is specified in board
	* configuration.
	*
	* @return bool
	*/
	public function should_run()
	{
		return (int) $this->config['update_rate_last_gc'] < time() - (int) $this->config['update_rate_gc'];
	}
}
