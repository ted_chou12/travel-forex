<?php

namespace phpbb;

use \phpbb\config\config;
use \phpbb\db\driver\driver_interface;

class rate
{
	/**
	* Constructor
	*
	* @param	\phpbb\template\template			$template
	* @param	\phpbb\user							$user
	* @param	\phpbb\controller\helper			$helper
	* @param	\phpbb\event\dispatcher_interface	$phpbb_dispatcher
	*/
	public function __construct(config $config, driver_interface $db, \phpbb\user $user, \phpbb\controller\helper $helper, \phpbb\event\dispatcher_interface $phpbb_dispatcher)
	{
		$this->config = $config;
		$this->db = $db;
		$this->user = $user;
		$this->helper = $helper;
		$this->phpbb_dispatcher = $phpbb_dispatcher;
	}

	/**
	* Generate a pagination link based on the url and the page information
	*
	* @param string $base_url is url prepended to all links generated within the function
	*							If you use page numbers inside your controller route, base_url should contains a placeholder (%d)
	*							for the page. Also be sure to specify the pagination path information into the start_name argument
	* @param string $on_page is the page for which we want to generate the link
	* @param string $start_name is the name of the parameter containing the first item of the given page (example: start=20)
	*							If you use page numbers inside your controller route, start name should be the string
	*							that should be removed for the first page (example: /page/%d)
	* @param int $per_page the number of items, posts, etc. to display per page, used to determine the number of pages to produce
	* @return string URL for the requested page
	*/
	protected function get_current_rate()
	{
		$sql = 'SELECT * FROM ' . RATE_TABLE . ' ORDER BY rate_datetime DESC LIMIT 0, 1';
		$result = $this->db->sql_query($sql);
		$rows = $this->db->sql_fetchrowset($result);
		$latest_rate = $rows[0]['rate_price'];

		if (!$this->config['update_rate_gc']) {
			$this->config->set('update_rate_gc', 3600);
		}

		return $latest_rate;
	}

	/**
	* Generate template rendered pagination
	*/
	public function get_sell_rate()
	{
		return $this->get_current_rate() + 0.002;
	}

	/**
	* Get current page number
	*
	* @param int $per_page the number of items, posts, etc. per page
	* @param int $start the item which should be considered currently active, used to determine the page we're on
	* @return int	Current page number
	*/
	public function get_buy_rate()
	{
		return $this->get_current_rate() - 0.002;
	}

	/**
	* Update Rate
	*
	* @param int $per_page the number of items, posts, etc. per page
	* @param int $start the item which should be considered currently active, used to determine the page we're on
	* @return int	Current page number
	*/
	public function update_rate($ntd, $jpy, $latest_rate)
	{
		$sql = 'INSERT INTO ' . RATE_TABLE . ' ' . $this->db->sql_build_array('INSERT', array(
			'rate_datetime'		=> date('Y-m-d H:i:s'),
			'rate_twd'		=> (float) round($ntd, 4),
			'rate_jpy'		=> (float) round($jpy, 4),
			'rate_price'		=> (float) $latest_rate)
		);
		
		$this->db->sql_query($sql);
	}
}
